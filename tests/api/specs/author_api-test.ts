import { expect } from "chai";
import { checkResponseTime, checkStatusCode, checkSchemas, checkName, checkBodyLenght } from '../../helpers/functionsForChecking.helper';
import { AuthorController } from "../lib/controllers/author.controller";
const author = new AuthorController();
const schemas = require('./data/schemas_testData.json');
var chai = require('chai');
chai.use(require('chai-json-schema'));


describe('Author Controller | with hooks', () => {
    let mytoken: string, authorId: string;
    let idarticle: string;
    
    before(`should get access token and get user id`, async () => {
        // runs once before the first test in this block
        let response = await author.postAuthorLogin('gogatest26@gmail.com', 'gaGonyh3426S');
        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        checkSchemas(response, schemas.schema_login);
        mytoken = response.body.accessToken;

        response = await author.getAuthor(mytoken);
        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        checkSchemas(response, schemas.schema_getAuthor);
        authorId = response.body.id;
    });

    it(`postAuthor`, async () => {
        let authorInfo = {
            id: authorId,
            userId: "aea89b97-d38b-459e-89ab-b0751975496e",
            avatar: null,
            firstName: "Mary",
            lastName: "Smith",
            job: null,
            location: "Andorra",
            company: "QA",
            website: "",
            twitter: "https://twitter.com/",
            biography: ""
        };

        let response = await author.postAuthor(mytoken, authorInfo);
        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        checkSchemas(response, schemas.schema_successMessage);    
    });

    it(`getUser`, async () => {
        let response = await author.getUser(mytoken);
        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        checkSchemas(response, schemas.schema_getUser);
    });

    it(`userIdAuthor`, async () => {
        let response = await author.userIdAuthor(mytoken, authorId);
        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        checkSchemas(response, schemas.schema_userIdAuthor);
        checkName(response);
    });

    it(`postArticle`, async () => {
        let newArticle = {    
            authorId: authorId,
            authorName: "Mary Smith",
            image: "https://knewless.tk/assets/images/8fc6dc5f-3e15-419a-b076-c44cd83284d5.jpg",
            name: "Song: Have a Cigar",
            text: `Come in here, dear boy, have a cigar
                    You're gonna go far, you're gonna fly
                    You're never gonna die
                    You're gonna make it if you try
                    They're gonna love you...`    
           
        };
        let response = await author.postArticle(mytoken, newArticle);
        
        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        checkSchemas(response, schemas.schema_postArticle);
        idarticle = response.body.id; 
    });
    it(`getArticleAuthor`, async () => {
        let response = await author.getArticleAuthor(mytoken);
        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        checkSchemas(response, schemas.schema_getArticleAuthor);
        checkBodyLenght(response);   
    });
    it(`getArticleID`, async () => {
        let response = await author.getArticleID(mytoken, idarticle);
        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        checkSchemas(response, schemas.schema_getArticleId);   
    });
    it(`postComment`, async () => {
        let newComment = {
            articleId: idarticle,
            text: "Nice song ever, fascinating band!"            
        }
        let response = await author.postComment(mytoken, newComment);
        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        checkSchemas(response, schemas.schema_postArticleComment);
    });
    it(`getComment`, async () => {
        let response = await author.getComment(mytoken, idarticle);
        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        checkSchemas(response, schemas.schema_getArticleComment);
      
    });
    afterEach(function () {
        console.log('It was a test');
    });

})