import { expect } from "chai";
import { checkResponseTime, checkStatusCode, checkSchemas } from '../../helpers/functionsForChecking.helper';
import { StudentController } from "../lib/controllers/student.controller";
const student = new StudentController();
const schemas = require('./data/schemas_testData.json');
var chai = require('chai');
chai.use(require('chai-json-schema'));

describe('Student Controller | with hooks', () => {
    let studentToken: string, studentId: string;
    let courseId: string;
    
    before(`Should get access token and get user id`, async () => {

        let response = await student.postStudentLogin('guitarbassok@gmail.com', 'XgJXxFljG8sV8asR');
        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        checkSchemas(response, schemas.schema_login);
        studentToken = response.body.accessToken;

        response = await student.getStudent(studentToken);
        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        checkSchemas(response, schemas.schema_getStudent);
        studentId = response.body.id;
    });

    it(`postStudent`, async () => {
        let updateStudent = {
                avatar: "https://knewless.tk/assets/images/bebb2284-6af1-4155-866c-fe9ff960918e.jpg",
                biography: "Nothing special. Just live in Lviv",
                company: "QA Agency",
                direction: "Developer",
                education: "Master's degree",
                employment: "Employee",
                experience: 1,
                firstName: "Laura",
                id: "ac56dab2-be31-493f-bdcd-d35d5432e6e2",
                industry: "Web Services",
                job: "QA",
                lastName: "Lopez",
                level: "Intermediate",
                location: "Ukraine",
                role: "QA",
                tags: [],
                userId: "995109a6-7f86-4c36-8b9d-b565f3b7bbdc",
                website: "https://artpole.org/",
                year: 1996
        };
        let response = await student.postStudent(studentToken, updateStudent);
        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        checkSchemas(response, schemas.schema_successMessage);      
    });

    it(`getCourse`, async () => {
        let response = await student.getCourse(studentToken);
        courseId = response.body[7].id;
        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        checkSchemas(response, schemas.schema_getCourse);
    });

    it(`getCourseInfo`, async () => {
        let response = await student.getCourseInfo(studentToken, courseId);
        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        checkSchemas(response, schemas.schema_getCourseInfo); 
    });

    it(`postCourseToFavorite`, async () => {
        let favorite ={
            id: courseId,
            type: "COURSE"
        }
        let response = await student.postCourseToFavorite(studentToken, courseId, favorite);
        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
    });

    it(`favoriteCourses`, async () => {
        let response = await student.favoriteCourses(studentToken);
        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        checkSchemas(response, schemas.schema_favoriteCourses);   
    });
    afterEach(function () {
        console.log('It was a test');
    });

})