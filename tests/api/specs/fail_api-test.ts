import { expect } from "chai";
import { checkResponseTime, checkStatusCode, checkResponseBodyError, checkResponseBodyMessage } from '../../helpers/functionsForChecking.helper';
import { FailController } from "../lib/controllers/fail.controller";
const fail = new FailController();
const schemas = require('./data/schemas_testData.json');
var chai = require('chai');
chai.use(require('chai-json-schema'));

describe("Fail Controller", () => {

    it(`Fail get Student`, async () => {
        let response = await fail.getStudent();
        console.log(response.body);
        checkStatusCode(response, 401);
        checkResponseBodyError(response, 'Unauthorized');
        checkResponseBodyMessage(response, '');
        checkResponseTime(response, 3000);
    });

    it(`Fail get Course`, async () => {
        let response = await fail.getCourse();
        checkStatusCode(response, 401);
        checkResponseBodyError(response, 'Unauthorized');
        checkResponseBodyMessage(response, '');
        checkResponseTime(response, 3000);
    });

    it(`Fail favorite Courses`, async () => {
        let response = await fail.favoriteCourses();
        checkStatusCode(response, 401);
        checkResponseBodyError(response, 'Unauthorized');
        checkResponseBodyMessage(response, '');
        checkResponseTime(response, 3000);
      
    });
    afterEach(function () {
        console.log('It was a test');
    });

})