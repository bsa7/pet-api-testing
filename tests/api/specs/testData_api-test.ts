import {
    checkResponseTime,
    checkStatusCode,
    checkResponseBodyStatus,
    checkResponseBodyMessage,
} from '../../helpers/functionsForChecking.helper';
import { AuthorController } from '../lib/controllers/author.controller';
import { StudentController } from "../lib/controllers/student.controller";
const author = new AuthorController();
const student = new StudentController();

describe('Use test data', () => {
    let authorInvalidCredentialsDataSet = [
        { email: 'gogatest26@gmail.com', password: '      ' },
        { email: 'gogatest26@gmail.com', password: 'admin ' },
        { email: 'gogatest26@gmail.com', password: 'gaGonyh 3426S' },
        // { email: 'gogatest26@gmail.com', password: 'admin' },
        // { email: 'gogatest26@gmail.com', password: 'gogatest26@gmail.com' },
        // { email: 'gogatest26@gmail.com ', password: 'gaGonyh3426S' },
        // { email: 'gogatest26@gmail.com  ', password: 'gaGonyh3426S' },
    ];

    let studentInvalidCredentialsDataSet =[
        { email: 'guitarbassok@gmail.com', password: '      ' },
        { email: 'guitarbassok@gmail.com', password: 'XgJXxFljG8sV8asR ' },
        { email: 'guitarbassok@gmail.com', password: 'XgJXxFljG8sV 8asR' },
        // { email: 'guitarbassok@gmail.com', password: 'admin' },
        // { email: 'guitarbassok@gmail.com', password: 'guitarbassok@gmail.com' },
        // { email: 'guitarbassok@gmail.com ', password: 'XgJXxFljG8sV8asR' },
        // { email: 'guitarbassok@gmail.com  ', password: 'XgJXxFljG8sV8asR' },
    ];

    authorInvalidCredentialsDataSet.forEach((credentials) => {
        it(`login using invalid credentials : ${credentials.email} + ${credentials.password}`, async () => {
            let response = await author.postAuthorLogin(credentials.email, credentials.password);

            checkStatusCode(response, 401);
            checkResponseBodyStatus(response, 'UNAUTHORIZED');
            checkResponseBodyMessage(response, 'Bad credentials');
            checkResponseTime(response, 3000);
        });
    });

    studentInvalidCredentialsDataSet.forEach((credentials) => {
        it(`login using invalid credentials : ${credentials.email} + ${credentials.password}`, async () => {
            let response = await student.postStudentLogin(credentials.email, credentials.password);

            checkStatusCode(response, 401);
            checkResponseBodyStatus(response, 'UNAUTHORIZED');
            checkResponseBodyMessage(response, 'Bad credentials');
            checkResponseTime(response, 3000);
        });
    });
    afterEach(function () {
        console.log('It was a test of data sets');
    });
   
});
