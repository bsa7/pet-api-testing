import { ApiRequest } from "../request";

let baseUrl: string = global.appConfig.baseUrl;
export class AuthorController {   
    async postAuthorLogin(emailAuth: string, passwordAuth: string){
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST") 
            .url(`auth/login`) 
            .body({
                email: emailAuth,
                password: passwordAuth
            })
            .send();
        return response;
    }

    async getAuthor(mytoken: string){
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET") 
            .url(`author`) 
            .bearerToken(mytoken)
            .send();
        return response;
    }

    async postAuthor(mytoken: string, authorObj: object){
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST") 
            .url(`author`) 
            .bearerToken(mytoken)
            .body(authorObj)
            .send();
        return response;
    }

    async getUser(mytoken: string){
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET") 
            .url(`user/me`) 
            .bearerToken(mytoken)
            .send();
        return response;
    }

    async userIdAuthor(mytoken: string, authorId: string){
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET") 
            .url(`author/overview/${authorId}`) 
            .bearerToken(mytoken)
            .send();
        return response;
    }

    async postArticle(mytoken: string, articleObj: object){
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST") 
            .url(`article`) 
            .bearerToken(mytoken)
            .body(articleObj)
            .send();
        return response;
    }

    async getArticleAuthor(mytoken: string){
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET") 
            .url(`article/author`) 
            .bearerToken(mytoken)
            .send();
        return response;
    }

    async getArticleID(mytoken: string, id: string){
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET") 
            .url(`article/${id}`) 
            .bearerToken(mytoken)
            .send();
        return response;
    }

    async postComment(mytoken: string, commentObj: object){
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST") 
            .url(`article_comment`) 
            .bearerToken(mytoken)
            .body(commentObj)
            .send();
        return response;
    }
    async getComment(mytoken: string, id: string){
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET") 
            .url(`article_comment/of/${id}?size=200`) 
            .bearerToken(mytoken)
            .send();
        return response;
    }

}