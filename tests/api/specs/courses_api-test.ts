import { expect } from "chai";
import { checkResponseTime, checkStatusCode, checkSchemas, checkBodyLenght } from '../../helpers/functionsForChecking.helper';
import { CoursesController } from "../lib/controllers/courses.controller";
const courses = new CoursesController();
const schemas = require('./data/schemas_testData.json');
var chai = require('chai');
chai.use(require('chai-json-schema'));

describe('Courses controller', () => {
    let courseId: string;

    it(`getAllCourses`, async () => {
        let response = await courses.getAllCourses();

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        expect(response.body.length).to.be.greaterThan(1);

        courseId = response.body[0].id;
        console.log(courseId);
    });

    it(`getPopularCourses`, async () => {
        let response = await courses.getPopularCourses();

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkBodyLenght(response);
    });

    it(`getCourseDetails`, async () => {
        let response = await courses.getAllCourseInfoById(courseId);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkSchemas(response, schemas.schema_courseInfo);
    });
    afterEach(function () {
        console.log('It was a test');
    });
});

