import { ApiRequest } from "../request";

let baseUrl: string = global.appConfig.baseUrl;

export class FailController {  
    async getStudent(){  
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET") 
            .url(`student`) 
            .bearerToken()
            .send();
        return response;
    }

    async getCourse(){   
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET") 
            .url(`path`) 
            .bearerToken()
            .send();
        return response;
    }

    async favoriteCourses(){  
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET") 
            .url(`favorite/courses`) 
            .bearerToken()
            .send();
        return response;
    }

}