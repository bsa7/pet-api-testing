import { ApiRequest } from "../request";
let baseUrl: string = global.appConfig.baseUrl;

export class StudentController { 
    async postStudentLogin(emailStudent: string, passwordStudent: string){
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST") 
            .url(`auth/login`) 
            .body({
                email: emailStudent,
                password: passwordStudent       
            })
            .send();
        return response;
    }

    async getStudent(studentToken: string){
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET") 
            .url(`student`) 
            .bearerToken(studentToken)
            .send();
        return response;
    }

    async postStudent(studentToken: string, studentObj: object){
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST") 
            .url(`student`) 
            .bearerToken(studentToken)
            .body(studentObj)
            .send();
        return response;
    }
    async getCourse(studentToken: string){ 
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET") 
            .url(`course`) 
            .bearerToken(studentToken)
            .send();
        return response;
    }
    async getCourseInfo(studentToken: string, courseID: string){
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET") 
            .url(`course/${courseID}/info`) 
            .bearerToken(studentToken)
            .send();
        return response;
    }

    async postCourseToFavorite(studentToken: string, courseID: string, favObj: object ){
            const response = await new ApiRequest()    
            .prefixUrl(baseUrl)
            .method("POST") 
            .url(`favorite/change?id=${courseID}&type=COURSE`) 
            .bearerToken(studentToken)
            .body(favObj)
            .send();
        return response;
    }

    async favoriteCourses(studentToken: string){
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET") 
            .url(`favorite/courses`) 
            .bearerToken(studentToken)
            .send();
        return response;
    }

}